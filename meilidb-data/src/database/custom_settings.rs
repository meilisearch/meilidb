use std::collections::{HashMap, HashSet};
use std::ops::Deref;
use crate::database::raw_index::InnerRawIndex;
use serde::de::DeserializeOwned;
use serde::{Serialize, Deserialize};
use super::Error;

#[derive(Debug, Copy, Clone, PartialEq, Eq, Serialize, Deserialize)]
#[serde(rename_all = "lowercase")]
pub enum RankingOrdering {
    Asc,
    Dsc
}

pub type StopWords = HashSet<String>;
pub type RankingOrder = Vec<String>;
pub type DistinctField = String;
pub type RankingRules = HashMap<String, RankingOrdering>;


#[derive(Clone)]
pub struct CustomSettings(pub(crate) InnerRawIndex);

impl Deref for CustomSettings {
    type Target = InnerRawIndex;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl CustomSettings {
	fn get<T: DeserializeOwned, K: AsRef<[u8]>>(&self, key: K) -> Result<Option<T>, Error> {
        let setting = self.0.get(key)?;
        let raw = match setting {
            Some(raw) => raw,
            None => return Ok(None)
        };

        Ok(Some(bincode::deserialize(&raw)?))
    }

    fn set<T: Serialize, K: AsRef<[u8]>>(&self, key: K, data: &T) -> Result<(), Error> {
        let raw = bincode::serialize(data)?;
        self.0.set(key, &raw)?;
        Ok(())
    }

    pub fn get_stop_words(&self) -> Result<Option<StopWords>, Error> {
    	self.get("stop_words")
    }

    pub fn get_ranking_order(&self) -> Result<Option<RankingOrder>, Error> {
    	self.get("ranking_order")
    }

    pub fn get_distinct_field(&self) -> Result<Option<DistinctField>, Error> {
    	self.get("distinct_field")
    }

    pub fn get_ranking_rules(&self) -> Result<Option<RankingRules>, Error> {
    	self.get("ranking_rules")
    }

    pub fn set_stop_words(&self, value: StopWords) -> Result<(), Error> {
    	self.set("stop_words", &value)
    }

    pub fn set_ranking_order(&self, value: RankingOrder) -> Result<(), Error> {
    	self.set("ranking_order", &value)
    }

    pub fn set_distinct_field(&self, value: DistinctField) -> Result<(), Error> {
    	self.set("distinct_field", &value)
    }

    pub fn set_ranking_rules(&self, value: RankingRules) -> Result<(), Error> {
    	self.set("ranking_rules", &value)
    }
}


