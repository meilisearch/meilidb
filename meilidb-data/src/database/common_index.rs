use core::fmt::Debug;
use std::sync::Arc;
use serde::de::DeserializeOwned;
use serde::Serialize;
use super::Error;
use std::marker::PhantomData;

#[derive(Clone)]
pub struct CommonIndex(pub Arc<rocksdb::DB>);

impl CommonIndex {
	pub fn get<T: DeserializeOwned + Debug, K: AsRef<[u8]>>(&self, key: K) -> Result<Option<T>, Error> {
        let cf = self.0.cf_handle("default-common").expect("cf not found");
        let raw = match self.0.get_cf(cf, key)? {
            Some(raw) => raw,
            None => return Ok(None)
        };
        let data = bincode::deserialize(&raw)?;
        Ok(Some(data))
    }

    pub fn set<T: Serialize + Debug, K: AsRef<[u8]>>(&self, key: K, data: &T) -> Result<(), Error> {
        let cf = self.0.cf_handle("default-common").expect("cf not found");
        let raw = bincode::serialize(data)?;
        self.0.put_cf(cf, key, &raw)?;
        Ok(())
    }

    pub fn delete<K: AsRef<[u8]>>(&self, key: K) -> Result<(), Error> {
        let cf = self.0.cf_handle("default-common").expect("cf not found");
        self.0.delete_cf(cf, key)?;
        Ok(())
    }

    pub fn prefix_iterator<T: DeserializeOwned, P: AsRef<[u8]>>(&self, prefix: P) -> Result<SerializedIterator<T>, rocksdb::Error> {
        let cf = self.0.cf_handle("default-common").expect("cf not found");
        let iter = self.0.prefix_iterator_cf(cf, prefix)?;
        
        Ok(SerializedIterator {
            iter,
            _marker: PhantomData,
        })
    }
}


pub struct SerializedIterator<'a, T>{
    iter: rocksdb::DBIterator<'a>,
    _marker: PhantomData<T>,
}

impl<'a, T> Iterator for SerializedIterator<'a, T> 
where T: DeserializeOwned 
{
    type Item = (String, T);
    
    fn next(&mut self) -> Option<Self::Item> {
        let (raw_key, raw_value) = match self.iter.next() {
            Some((key, value)) => (key, value),
            None => return None
        };

        let value: T = match bincode::deserialize(&raw_value) {
            Ok(data) => data,
            Err(_) => return None,
        };

        let key = match std::str::from_utf8(&raw_key) {
            Ok(key) => key.to_string(),
            Err(_) => return None
        };

        Some((key, value))
    }
}